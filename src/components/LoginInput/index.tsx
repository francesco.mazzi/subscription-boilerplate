import "./loginInput.css";
import { useState, useRef, useEffect } from "react";
//firebase
import {
  getAuth,
  onAuthStateChanged,
  GoogleAuthProvider,
  createUserWithEmailAndPassword,
  signInWithPopup,
  signOut,
} from "@firebase/auth";

import { initializeApp } from "firebase/app";
import firebaseConfig from "../../config/config";

initializeApp(firebaseConfig);

const LoginInput: React.FC = () => {
  const auth = getAuth();

  //   const inputRef = useRef<HTMLInputElement>(null);
  const emailRef = useRef<HTMLInputElement>(null);
  const pwdRef = useRef<HTMLInputElement>(null);
  //form setup
  const [registering, setRegistering] = useState(false);
  const [userMail, setUserMail] = useState("");
  const [pwd, setPwd] = useState("");
  const [confirm, setConfirm] = useState("");
  const [error, setError] = useState("");

  //autocheck setup
  const [loading, setLoading] = useState(false);

  //google login setup
  const [authing, setAuthing] = useState(false);

  //user pwd function
  const signUpWithEmailAndPassword = () => {
    //Check data error
    // if (pwd !== confirm) setError("❌Controlla la password sia corretta");
    // if (error !== "") setError("");
    // setRegistering(true);
    // createUserWithEmailAndPassword(auth, userMail, pwd)
    //   .then((userCredential) => {
    //     logging.info(userCredential);
    //     // Signed in
    //     const user = userCredential.user;
    //     //history.push('/login');
    //     console.log("Login!");
    //     console.log(user);
    //   })
    //   .catch((error) => {
    //     logging.error(error);
    //     console.log(error);
    //     if (error.code.includes("auth/weak-password")) {
    //       setError("Inserisci una password più forte");
    //     } else if (error.code.includes("auth/email-already-in-use")) {
    //       setError("Email già in uso");
    //     }
    //     setRegistering(false);
    //   });
  };

  //google login function
  const signiInWithGoogle = async () => {
    setAuthing(true);

    signInWithPopup(auth, new GoogleAuthProvider())
      .then((response) => {
        console.log(response.user.uid);
        //navigate('/'); -> o un'altra pg che preferiamo
        setLoading(true);
      })
      .catch((error) => {
        console.log(error);
        setAuthing(false);
      });
  };

  useEffect(() => {
    AuthCheck();
  }, [auth]);

  // azione simile a subscrbtion
  const AuthCheck = onAuthStateChanged(auth, (user) => {
    if (user) {
      setLoading(false);
    } else {
      console.log("Errore di accesso");
    }
  });

  return (
    <div className="container">
      <div className="form__container">
        {!loading ? (
          <form className="login__container">
            <h2>Iscriviti subito per la prova gratutia 👋🏻</h2>
            <input
              ref={emailRef}
              // value={userMail}
              type="text"
              className="input__box"
              placeholder="Inserisci la tua email"
            />
            <input
              ref={pwdRef}
              // value={pwd}
              type="text"
              className="input__box"
              placeholder="Inserisci la tua password"
            />
            <input
              ref={pwdRef}
              // value={pwd}
              type="text"
              className="input__box"
              placeholder="Conferma la tua password"
            />
            <div>
              {/* <button className="input__submit" type="submit">
                Sono già iscritto
              </button> */}
              <button
                className="input__submit"
                type="submit"
                onClick={() => signUpWithEmailAndPassword()}
              >
                Iscriviti
              </button>
              <button
                className="input__submit"
                type="submit"
                onClick={() => signiInWithGoogle()}
                disabled={authing}
              >
                Iscriviti con google
              </button>
            </div>
          </form>
        ) : (
          <div>
            <h2>{userMail.split("@")[0]} benvenuto! 👋🏻</h2>
            <button
              className="input__submit"
              type="submit"
              onClick={() => signOut(auth)}
            >
              Logout
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

export default LoginInput;
